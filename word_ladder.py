import re
def same(item, target):
  return len([c for (c, t) in zip(item, target) if c == t])

def build(pattern, words, seen, list):
  return [word for word in words
                 if re.search(pattern, word) and word not in seen.keys() and
                    word not in list]

def find(word, words, seen, target, path, prevMatch): #HERE
  list = []
  for i in range(len(word)):
    list += build(word[:i] + "." + word[i + 1:], words, seen, list)
  if len(list) == 0:
    return False
  list = sorted([(same(w, target), w) for w in list])[::-1]
  (match, item) = list[0]
  for (match, item) in list:
    if match >= len(target) - 1:
      if match == len(target) - 1:
        path.append(item)
      return True
    seen[item] = True
  for (match, item) in list:
    path.append(item)
    if match >= prevMatch and find(item, words, seen, target, path, match): #HERE
      return True
    path.pop()

fname = "dictionary.txt"
file = open(fname)
lines = file.readlines()
while True:
  start = input("Enter start word:")
  words = []
  for line in lines:
    word = line.rstrip()
    if len(word) == len(start):
      words.append(word)
  target = input("Enter target word:")
  middle = input("Enter a word that must exist (optional):")
  banned = input("Enter banned word (optional):")
  break

count = 0
path = [start]
seen = {start: True}
for word in banned.split(","):                              #       split converts banned into a list and adds a "," to seperate the words
  seen[word] = True

if middle:                                                  #       If middle exists, run the code to find start > middle > target.
    if find(start, words, seen, middle, path, 0):           #       this highlights the path to the middle word
      path.append(middle)
    else:
      print("No path found")                                #       Prints no path found if it can't be located
    if find(middle, words, seen, target, path, 0):          #       This finds the path from middle to target and prints
      path.append(target)
      print(len(path) - 1, path)
    else:
      print("No path found")                                #       Prints no path found if it can't be located
else:                                                       #       Else run start > target and print
    if find(start, words, seen, target, path, 0):
        path.append(target)
        print(len(path) - 1, path)